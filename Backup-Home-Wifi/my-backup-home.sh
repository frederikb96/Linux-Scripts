#!/bin/bash

# Documentation for initialization
# kopia repository create sftp --path=/mnt/backup/backup-$NAME --host=backup.lan --username=root --keyfile=${HOME}/.ssh/id_rsa --known-hosts=${HOME}/.ssh/known_hosts
# kopia policy set --add-ignore '*' ${HOME}/Nextcloud
# kopia policy set --add-ignore '*' ${HOME}/Programs/VirtualMaschine
# kopia policy set --global --add-ignore '.cache' --add-ignore 'cache' --add-ignore 'Trash'
# kopia policy set --global --keep-annual 1 --keep-monthly 3

# Variables
status_exit=0;
HOME="$1" # e.g. /home/freddy
if [[ $HOME == "" ]]; then
    echo "No backup directory provided!"
    exit 1;
fi
NAME="$2" # e.g. freddy-desktop
WIFI="$3" # e.g. GuckAmRouter

check_status() {
	status=$1
	if [ $status -ne 0 ]; then status_exit=1; fi
	echo "Result: $status"
}

main() {

echo -e "\n--------------------"
echo "Check Network"
echo -e "--------------------\n"
NET=$( iwgetid )
check_status $?
if [[ $NET != *${WIFI}* ]]; then
  echo "No backup since not in correct network!"
  notify-send -u low "Backup <$HOME> Skipped" "Not in correct network $WIFI";
  exit 0
fi

echo -e "\n--------------------"
echo "Get ENV"
echo -e "--------------------\n"
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"
check_status $?

echo -e "\n--------------------"
echo "Connect Kopia"
echo -e "--------------------\n"
kopia repository connect sftp --path=/mnt/backup/backup-$NAME -p $KOPIA_PASSWORD --host=backup.lan --username=root --keyfile=${HOME}/.ssh/id_rsa --known-hosts=${HOME}/.ssh/known_hosts
check_status $?

echo -e "\n--------------------"
echo "Create App List:"
echo -e "--------------------\n"
echo "" > "${HOME}/.tmp-app-list.log"

echo "APT:" >> "${HOME}/.tmp-app-list.log"
apt list --installed >> "${HOME}/.tmp-app-list.log"
check_status $?

echo "Snap:" >> "${HOME}/.tmp-app-list.log"
snap list >> "${HOME}/.tmp-app-list.log"
check_status $?

echo "Flatpak:" >> "${HOME}/.tmp-app-list.log"
flatpak list >> "${HOME}/.tmp-app-list.log"
check_status $?

echo "Opt:" >> "${HOME}/.tmp-app-list.log"
ls -la /opt >> "${HOME}/.tmp-app-list.log"
check_status $?

echo -e "\n--------------------"
echo "Backup home:"
echo -e "--------------------\n"
if [ $status_exit == 0 ]; then
kopia snapshot create ${HOME}
check_status $?
fi

kopia snapshot list -a

echo -e "\n--------------------"
echo "Overall:"
echo -e "--------------------\n"
echo $status_exit

}

main > "${HOME}/.tmp-backup-home.log" 2>&1

if [ $status_exit -ne 0 ];
then notify-send -u critical "Backup <${HOME}> Error" "Check the log for errors: ~/.tmp-backup-home.log";
else notify-send "Backup <${HOME}> Completed";
fi

exit $status_exit
