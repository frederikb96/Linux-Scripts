#!/bin/bash

set -ex

source ./programs.sh

# Main
update
basic

# Surface
wget -qO - https://raw.githubusercontent.com/linux-surface/linux-surface/master/pkg/keys/surface.asc | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/linux-surface.gpg
echo "deb [arch=amd64] https://pkg.surfacelinux.com/debian release main" | sudo tee /etc/apt/sources.list.d/linux-surface.list
sudo apt update
sudo apt install -y linux-image-surface linux-headers-surface iptsd libwacom-surface
sudo systemctl enable iptsd
sudo apt install -y linux-surface-secureboot-mok
sudo update-grub
sudo apt install -y intel-microcode linux-firmware
sudo gedit /etc/ipts.conf

# Rest
flatpak
basic_more
nextcloud
tailscale
messenger
pdfstudio
firefox-esr
webapp

# Other
sudo snap install gitkraken --classic

# Update Mime
update-desktop-database /home/freddy/.local/share/applications/

