#!/bin/bash

set -ex

source ./programs.sh

# Main
sudo ubuntu-drivers autoinstall
update
basic
kopia
flatpak
basic_more
nextcloud

sudo flatpak install -y signal
sudo snap install discord

zotero
virtualbox

sudo apt install -y tikzit

sudo flatpak install -y scummvm
sudo flatpak install -y zoom
sudo apt install -y steam
sudo snap install gitkraken --classic
sudo snap install clion --classic
sudo snap install pycharm-professional --classic

sudo apt install anki
sudo apt install pdfsam
sudo apt install gimp

# Update Mime
update-desktop-database /home/nina/.local/share/applications
