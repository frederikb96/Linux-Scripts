#!/bin/bash

set -ex

source ./programs.sh

# Main
sudo ubuntu-drivers autoinstall
update
basic
kopia
flatpak
basic_more
ssh-server
nextcloud
tailscale
messenger
zotero
pdfstudio
firefox-esr
webapp

sudo apt install -y steam
sudo snap install gitkraken --classic
sudo snap install clion --classic
sudo snap install pycharm-professional --classic

# Update Mime
update-desktop-database /home/freddy/.local/share/applications
