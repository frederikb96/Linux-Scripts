#!/bin/bash

set -xe

update() {
	sudo apt update && sudo apt upgrade -y && sudo apt -y autoremove && sudo snap refresh
}

basic() {
	sudo apt install -y libcanberra-gtk-module build-essential git curl meld gnome-screenshot gnome-shell-extension-manager gnome-tweaks
}

kopia() {
	curl -s https://kopia.io/signing-key | sudo gpg --dearmor -o /usr/share/keyrings/kopia-keyring.gpg
	echo "deb [signed-by=/usr/share/keyrings/kopia-keyring.gpg] http://packages.kopia.io/apt/ stable main" | sudo tee /etc/apt/sources.list.d/kopia.list
	sudo apt update
	sudo apt -y install kopia
}

flatpak() {
	sudo apt install -y flatpak
	sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

basic_more() {
	sudo apt install -y kdeconnect python3 python3-pip adb fastboot gnome-sound-recorder pavucontrol gimp
	
	sudo add-apt-repository -y ppa:apandada1/xournalpp-stable
	sudo apt update
	sudo apt install -y xournalpp
	
	sudo flatpak install -y flathub org.gnome.Maps
	sudo flatpak install -y org.gnome.Solanum
}

ssh-server() {
	sudo apt install -y openssh-server
}

nextcloud() {
	sudo apt install -y nextcloud-desktop nautilus-nextcloud
}

sciebo() {
	wget -nv https://www.sciebo.de/install/linux/Ubuntu_22.04/Release.key -O - | sudo tee /etc/apt/trusted.gpg.d/sciebo.asc
	echo 'deb https://www.sciebo.de/install/linux/Ubuntu_22.04/ /' | sudo tee -a /etc/apt/sources.list.d/owncloud.list
	sudo apt update
	sudo apt install -y sciebo-client sciebo-client-nautilus
}

tailscale() {
	curl -fsSL https://tailscale.com/install.sh | sh
}

evolution() {
	sudo apt install -y evolution evolution-ews evolution-plugins
}

signal() {
	wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
	cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
	rm signal-desktop-keyring.gpg
	echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
	sudo apt update && sudo apt install -y signal-desktop
}

zoom() {
	sudo flatpak install -y us.zoom.Zoom
}

slack() {
	sudo snap install slack
}

element() {
	sudo apt install -y wget apt-transport-https
	sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
	echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
	sudo apt update
	sudo apt install -y element-desktop
}

messenger() {
	# Signal
	signal
	
	# Element
	element
	
	# Other
	zoom
	slack
}

zotero() {
	wget -qO- https://raw.githubusercontent.com/retorquere/zotero-deb/master/install.sh | sudo bash
	sudo apt update
	sudo apt install -y zotero
}

pdfstudio() {
	wget https://download.qoppa.com/pdfstudioviewer/PDFStudioViewer_linux64.deb && sudo apt install -y ./PDFStudioViewer_linux64.deb && rm PDFStudioViewer_linux64.deb
}

onlyoffice() {
	wget https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors_amd64.deb && sudo apt install -y ./onlyoffice-desktopeditors_amd64.deb && rm ./onlyoffice-desktopeditors_amd64.deb

}

firefox-esr() {
	sudo add-apt-repository -y ppa:mozillateam/ppa
	sudo apt install -y firefox-esr
}

webapp() {
	wget http://packages.linuxmint.com/pool/main/w/webapp-manager/webapp-manager_1.2.5_all.deb
	set +e
	sudo dpkg -i ./webapp-manager_1.2.5_all.deb
	set -e
	sudo apt --fix-broken install -y && rm webapp-manager_1.2.5_all.deb
}

virtualbox() {
	wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg
	echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian jammy contrib' | sudo tee -a /etc/apt/sources.list.d/virtualbox-jammy.list
	sudo apt-get update
	sudo apt install -y virtualbox-7.0
}

tex() {
	sudo apt install -y texlive-full texmaker tikzit
}

# Appimage
# libfuse2

# Other
# network-manager-openconnect-gnome  menulibre

#crossover, download https://media.codeweavers.com/pub/crossover/cxlinux/demo/crossover_23.5.0-1.deb, install a new bottle such that packages are update and installed, then you can start copied programs and bottles from other devices

#vs code, https://az764295.vo.msecnd.net/stable/f1b07bd25dfad64b0167beb15359ae573aecd2cc/code_1.83.1-1696982868_amd64.deb

# master pdf editor
https://code-industry.net/masterpdfeditor-help/package-installation-from-remote-repository/

master-pdf() {
	wget --quiet -O - http://repo.code-industry.net/deb/pubmpekey.asc | sudo tee /etc/apt/keyrings/pubmpekey.asc
	echo "deb [signed-by=/etc/apt/keyrings/pubmpekey.asc arch=$( dpkg --print-architecture )] http://repo.code-industry.net/deb stable main" | sudo tee /etc/apt/sources.list.d/master-pdf-editor.list
	sudo apt install master-pdf-editor-5
}

# other
# ausweisapp2 dconf-editor efibootmgr drawing etherwake ffmpeg gparted jq exiftool

# sudo snap install moonlight
# sudo snap install pycharm-professional --classic
# sudo snap install phpstorm --classic
# sudo snap install vidcutter