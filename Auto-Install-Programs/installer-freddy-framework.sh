#!/bin/bash

set -ex

source ./programs.sh

# Main
update
basic
kopia

# Rest
flatpak
basic_more
nextcloud
tailscale
signal
element
zoom
slack
zotero
tex



# Other
sudo snap install gitkraken --classic

sudo apt install -y libfuse2



# Update Mime
update-desktop-database /home/freddy/.local/share/applications/

