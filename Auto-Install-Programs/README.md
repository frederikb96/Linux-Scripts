These scripts can automate the installation of programs on a new device.
- The programs script is imported into the device specific scripts and offers funtions for different installation procedures
- The shortcut script can automatically assign custom shortcuts