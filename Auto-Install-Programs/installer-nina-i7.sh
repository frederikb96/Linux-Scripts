#!/bin/bash

set -ex

source ./programs.sh

# Main
sudo ubuntu-drivers autoinstall
update
basic
flatpak
basic_more
sciebo
nextcloud

signal
zoom
kopia

zotero
virtualbox

tex

sudo snap install gitkraken --classic
sudo snap install clion --classic
sudo snap install pycharm-professional --classic

sudo apt install pdfsam
sudo apt install gimp

# Update Mime
update-desktop-database /home/nina/.local/share/applications
