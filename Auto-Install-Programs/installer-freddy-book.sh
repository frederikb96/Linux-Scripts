#!/bin/bash

set -ex

source ./programs.sh

# Main
update
basic
kopia

# Rest
flatpak
basic_more
nextcloud
tailscale
signal
element
slack

pdfstudio
firefox-esr
webapp

# Other
sudo snap install gitkraken --classic

# Update Mime
update-desktop-database /home/freddy/.local/share/applications/

