#!/bin/bash

set -ex

source ./programs.sh

# Main
update
basic
kopia
flatpak
basic_more
evolution
nextcloud
sciebo
tailscale
messenger
sudo snap install telegram-desktop
zotero
pdfstudio
firefox-esr
webapp
virtualbox
tex
onlyoffice

# Other
sudo snap install gitkraken --classic
sudo snap install clion --classic
sudo snap install pycharm-professional --classic

sudo apt install -y openocd gcc-arm-none-eabi default-jre libncurses5 mesa-common-dev
sudo apt install -y wireshark tshark
sudo usermod -a -G wireshark frederik # $USER

sudo pam-auth-update # Fingerprint

# Update Mime
update-desktop-database /home/frederik/.local/share/applications/

# Post setup
echo "Post setup"
echo "vpn"
echo "grub"
