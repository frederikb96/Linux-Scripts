SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")

link() {
	relative="$1"
	if [[ $relative == "" ]]; then
		echo "No path to folder specified!"
		exit 1
	fi

	folder="$2"
	if [[ $folder == "" ]]; then
		echo "No folder name specified!"
		exit 1
	fi

	target_bin="$3"
	if [[ $target_bin == "" ]]; then
		target_bin="$HOME/.local/bin/my-bin"
	fi
	
	path_full="$SCRIPT_PATH/$relative/$folder"

	f_path_all=$path_full/*

	for f_path in $f_path_all; do
		f=$(basename $f_path)
		if [[ "$f" == *\.sh ]] || [[ "$f" == *\.py ]]; then
			ln -sf $f_path $target_bin/$f
		elif  [[ "$f" == *\.desktop ]]; then
			ln -sf $f_path $HOME/.local/share/applications/my-applications/$f
		elif  [[ "$f" == *\.png ]] || [[ "$f" == *\.jpg ]]; then
			ln -sf $f_path $HOME/.icons/$f
		fi
	done
}

link-app-custom() {
	link "../Apps-Custom" $1 $2
}

link-templates() {
	ln -s $SCRIPT_PATH/../Templates $HOME/Templates/my
}

reset() {
	$SCRIPT_PATH/reset.sh
}

show() {
	$SCRIPT_PATH/show.sh
}
