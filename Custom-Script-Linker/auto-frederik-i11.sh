#!/bin/bash

source ./auto-util.sh
reset

link-templates

link-app-custom "Obsidian"
link-app-custom "Tailscale"
link-app-custom "Update-Server-Remote"
link-app-custom "Util"
link-app-custom "Boxcryptor"
link-app-custom "ChibiStudio"
link-app-custom "Matlab"
link-app-custom "VM-Windows"
link-app-custom "Backup-i11"

show
