#!/bin/bash

source ./auto-util.sh
reset

link-templates

link-app-custom "Obsidian"
link-app-custom "Tailscale"
link-app-custom "Update-Server-Remote"
link-app-custom "Util"
link-app-custom "Windows-Dual-Boot"
link-app-custom "Backup-Sync"
link-app-custom "Backup-Home-Wifi"

show
