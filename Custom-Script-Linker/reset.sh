#!/bin/bash

rm -rf $HOME/.local/bin/my-bin
rm -rf $HOME/.local/share/applications/my-applications
rm -rf $HOME/.icons
rm -f $HOME/Templates/my

mkdir -p $HOME/.local/bin/my-bin
mkdir -p $HOME/.local/share/applications/my-applications
mkdir -p $HOME/.icons

