#!/bin/bash

source ./auto-util.sh

link-app-custom "Update-Server" "/usr/local/bin"
link-app-custom "Matrix" "/usr/local/bin"
link-app-custom "Backup-Server" "/usr/local/bin"
link-app-custom "Backup-Android" "/usr/local/bin"
link-app-custom "Networkdrive" "/usr/local/bin"

ls -la /usr/local/bin/
