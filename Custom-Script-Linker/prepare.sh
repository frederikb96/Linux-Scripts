#!/bin/bash

echo "Adds my-bin folder to PATH"

echo -e '## My custom stuff

# set PATH so it includes users private bin if it exists
if [ -d "$HOME/.local/bin/my-bin" ] ; then
    PATH="$HOME/.local/bin/my-bin:$PATH"
fi

# User anacron
/usr/sbin/anacron -s -t $HOME/.anacron/etc/anacrontab -S $HOME/.anacron/spool' >> $HOME/.profile

echo "Done, re-login needed"
read
