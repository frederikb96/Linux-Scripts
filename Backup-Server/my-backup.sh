#!/bin/bash

# Documentation for initialization
# kopia repository create filesystem --path=/mnt/backup/backup-ubuntu-server
# kopia policy set --global --add-ignore '.cache' --add-ignore 'cache' --add-ignore 'lost+found'
# kopia policy set --global --keep-annual 1 --keep-monthly 3
# kopia policy set --add-ignore '*' ${HOME}/.local/share/Trash

status_exit=0;

check_status() {
	status=$1
	if [ $status -ne 0 ]; then status_exit=1; fi
	echo "Result: $status"
}

check_exit() {
	status=$1
	if [ $status -ne 0 ]; then
	  echo "Subject:WARNING Backup failed for ubuntu-vserver" | sendmail fberg@posteo.de;
	  exit 1
	fi
	echo "Result: $status"
}

main() {

# Start
# Start
echo -e "\n--------------------"
echo "ENV:"
echo -e "--------------------\n"
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"

echo -e "\n--------------------"
echo "Connect:"
echo -e "--------------------\n"
kopia repository connect filesystem --path /mnt/backup/backup-server -p $KOPIA_PASSWORD
check_exit $?

echo -e "\n--------------------"
echo "Create App List:"
echo -e "--------------------\n"
echo "" > "${HOME}/.tmp-app-list.log"

echo "APT:" >> "${HOME}/.tmp-app-list.log"
apt list --installed >> "${HOME}/.tmp-app-list.log"
check_status $?

echo "Snap:" >> "${HOME}/.tmp-app-list.log"
snap list >> "${HOME}/.tmp-app-list.log"
check_status $?

echo -e "\n--------------------"
echo "Synapse DB:"
echo -e "--------------------\n"
mkdir -p /root/docker/databases
docker exec postgres-synapse pg_dumpall -U synapse > /root/docker/databases/docker-synapse-postgres.dump
check_status $?

echo -e "\n--------------------"
echo "Home:"
echo -e "--------------------\n"
kopia snapshot create /root
check_status $?

echo -e "\n--------------------"
echo "Status:"
echo -e "--------------------\n"
kopia snapshot list -a
kopia maintenance run
check_status $?

echo -e "\n--------------------"
echo "Overall:"
echo -e "--------------------\n"
echo $status_exit

}

main > "${HOME}/.tmp-backup.log" 2>&1

check_exit $status_exit

exit $status_exit
