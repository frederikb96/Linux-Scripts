#!/bin/bash

ssh root@pi wakeonlan A8:A1:59:D8:E5:76

while ( ! ssh root@pi ping -W 1 -c 1 192.168.0.20); do
    echo "Waiting for server to wake up..."
    sleep 1
done

ssh -t root@pi ssh root@192.168.0.20 -p 21

exit 0