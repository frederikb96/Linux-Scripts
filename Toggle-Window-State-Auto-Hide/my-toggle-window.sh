#!/bin/bash

# Get the name of a window
# It toggles the visibility of the window by hiding it completely (it is not just minimized, so also removed from open apps menu) and showing it again

# Example
# my-toggle-window.sh "Mozilla Thunderbird"
# my-toggle-window.sh "berg@embedded.rwth-aachen.de — Mozilla Firefox"
# my-toggle-window.sh "Nextcloud — Mozilla Firefox"

wid=$(xdotool search --limit 1 "$1")
if [ -z $wid  ] && [ "$2" != "" ]; then
	wid=$(xdotool search --limit 1 "$2")
fi
wstate=$(xwininfo -id $wid | grep "Map State:")

if [[ "$wstate" == *IsUnMapped ]]
then
  xdotool windowmap $wid windowactivate $wid
else
  xdotool windowunmap $wid
fi
