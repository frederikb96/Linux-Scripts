#!/bin/bash

echo "Updating to latest kernel"

latest_oem_kernel=$(ls /boot/vmlinuz-* | awk -F"-" '{split($0, a, "-"); version=a[3]; if (version>max) {max=version; kernel=a[2] "-" a[3] "-" a[4]}} END{print kernel}')
echo "latest_oem_kernel=$latest_oem_kernel"
sudo sed -i.bak '/^GRUB_DEFAULT=/c\GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux '"$latest_oem_kernel"'"' /etc/default/grub
sudo sed -i 's/^GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash loglevel=3 module_blacklist=hid_sensor_hub"/g' /etc/default/grub
echo "replaced"
sudo update-grub
echo "updated grub"