#!/bin/bash

# Documentation for initialization
# kopia repository create filesystem --path /mnt/backup/backup-android
# kopia policy set --add-ignore '*' /mnt/android/ignore-folder
# kopia policy set --global --add-ignore '.cache' --add-ignore 'cache'
# kopia policy set --global --keep-annual 1 --keep-monthly 3



status_exit=0;

check_status() {
	status=$1
	if [ $status -ne 0 ]; then status_exit=1; fi
	echo "Result: $status"
	return $status
}

clean() {
  umount -fl /mnt/android/data-data
  umount -fl /mnt/android/sdcard
}

error() {
	status=$1
	if [ $status -ne 0 ]; then
	  clean
	  echo "Error occurred: $status"
	  exit $status
  fi
  return $status
}


# Start
echo -e "\n--------------------"
echo "ENV:"
echo -e "--------------------\n"
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"
check_status $?
error $?

echo -e "\n--------------------"
echo "Connect Android:"
echo -e "--------------------\n"
clean
mkdir -p /mnt/android/data-data /mnt/android/sdcard
sshfs root@freddy-phone.lan:/data/data/ /mnt/android/data-data -p 2222
check_status $?
error $?
sshfs root@freddy-phone.lan:/sdcard/ /mnt/android/sdcard -p 2222
check_status $?
error $?

echo -e "\n--------------------"
echo "Connect Repo:"
echo -e "--------------------\n"
kopia repository connect filesystem --path /mnt/backup/backup-android -p $KOPIA_PASSWORD
check_status $?
error $?

echo -e "\n--------------------"
echo "Data:"
echo -e "--------------------\n"
kopia snapshot create /mnt/android/data-data/
check_status $?

echo -e "\n--------------------"
echo "Sdcard:"
echo -e "--------------------\n"
kopia snapshot create /mnt/android/sdcard/
check_status $?

echo -e "\n--------------------"
echo "Status:"
echo -e "--------------------\n"
kopia snapshot list -a
kopia maintenance run
check_status $?

echo -e "\n--------------------"
echo "Disconnect Android:"
echo -e "--------------------\n"
umount /mnt/android/data-data
check_status $?
umount /mnt/android/sdcard
check_status $?


echo -e "\n--------------------"
echo "Overall:"
echo -e "--------------------\n"
echo $status_exit

exit $status_exit
