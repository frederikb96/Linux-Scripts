#!/bin/bash

# Documentation
# Get the desired app that needs to be restored
# Get from android the real uid and gid
# Restore the directories available for this app and do for every:
# restore uid and gid
# rsync back to android

# clean up


status_exit=0;


check_status() {
	status=$1
	if [ $status -ne 0 ]; then status_exit=1; fi
	echo "Result: $status"
	return $status
}

clean() {
  umount -fl /mnt/android/data-data
  umount -fl /mnt/android/sdcard
  rm -rf /tmp/android-restore
}

error() {
	status=$1
	if [ $status -ne 0 ]; then
	  clean
	  echo "Error occurred: $status"
	  exit $status
  fi
  return $status
}


# Start
echo -e "\n--------------------"
echo "Get desired app name:"
echo -e "--------------------\n"
app="$1"
echo "$app"
if [ "$app" == "" ] || [ "$app" == " " ]; then
  echo "Error: No app name provided!"
  error 1
fi

echo -e "\n--------------------"
echo "ENV:"
echo -e "--------------------\n"
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"
check_status $?
error $?

echo -e "\n--------------------"
echo "Connect Repo:"
echo -e "--------------------\n"
kopia repository connect filesystem --path /mnt/backup/backup-android -p $KOPIA_PASSWORD
check_status $?
error $?

echo -e "\n--------------------"
echo "Connect Android:"
echo -e "--------------------\n"
sshfs root@freddy-phone.lan:/data/data/ /mnt/android/data-data -p 2222
check_status $?
error $?
sshfs root@freddy-phone.lan:/sdcard/ /mnt/android/sdcard -p 2222
check_status $?
error $?

echo -e "\n--------------------"
echo "Get uid and gid:"
echo -e "--------------------\n"
id=$(stat -c "%g" /mnt/android/data-data/${app})
check_status $?
error $?
idc=$(( $id+10000 ))
check_status $?
error $?

echo -e "\n--------------------"
echo "Restore Data:"
echo -e "--------------------\n"
# u=id, g=id
# cache gets g=idc
if [ ! -d "/mnt/android/data-data/${app}" ]; then
  echo "Error: No data directory!"
  error 1
fi
mkdir -p "/tmp/android-restore/data"
kopia snapshot restore --snapshot-time=latest "/mnt/android/data-data/${app}" "/tmp/android-restore/data/${app}"
chmod 700 "/tmp/android-restore/data/${app}"
chown -R ${id}:${id} "/tmp/android-restore/data/${app}"
chown -R ${id}:${idc} "/tmp/android-restore/data/${app}/cache"
chown -R ${id}:${idc} "/tmp/android-restore/data/${app}/code_cache"
rsync -av /tmp/android-restore/data/${app}/ /mnt/android/data-data/${app}/
check_status $?
error $?

rm -rf /tmp/android-restore/data

echo -e "\n--------------------"
echo "Restore sdcard/data:"
echo -e "--------------------\n"
# u=id, g=1015
mkdir -p "/tmp/android-restore/sdcard/data"
kopia snapshot restore --snapshot-time=latest "/mnt/android/sdcard/Android/data/${app}" "/tmp/android-restore/sdcard/data/${app}"
if [ $? -eq 0 ]; then
  chown -R ${id}:1015 "/tmp/android-restore/sdcard/data/${app}"
  rsync -av /tmp/android-restore/sdcard/data/${app}/ /mnt/android/sdcard/Android/data/${app}/
  check_status $?
fi
rm -rf /tmp/android-restore/sdcard/data

echo -e "\n--------------------"
echo "Restore sdcard/media:"
echo -e "--------------------\n"
# u=id, g=9997
mkdir -p "/tmp/android-restore/sdcard/media"
kopia snapshot restore --snapshot-time=latest "/mnt/android/sdcard/Android/media/${app}" "/tmp/android-restore/sdcard/media/${app}"
if [ $? -eq 0 ]; then
  chown -R ${id}:9997 "/tmp/android-restore/sdcard/media/${app}"
  rsync -av /tmp/android-restore/sdcard/media/${app}/ /mnt/android/sdcard/Android/media/${app}/
  check_status $?
fi

rm -rf /tmp/android-restore/sdcard/media

echo -e "\n--------------------"
echo "Restore sdcard/obb:"
echo -e "--------------------\n"
# u=id, g=1015
mkdir -p "/tmp/android-restore/sdcard/obb"
kopia snapshot restore --snapshot-time=latest "/mnt/android/sdcard/Android/obb/${app}" "/tmp/android-restore/sdcard/obb/${app}"
if [ $? -eq 0 ]; then
  chown -R ${id}:1015 "/tmp/android-restore/sdcard/obb/${app}"
  rsync -av /tmp/android-restore/sdcard/obb/${app}/ /mnt/android/sdcard/Android/obb/${app}/
  check_status $?
fi

rm -rf /tmp/android-restore/sdcard/obb

echo -e "\n--------------------"
echo "Clean up:"
echo -e "--------------------\n"
clean

echo -e "\n--------------------"
echo "Overall:"
echo -e "--------------------\n"
echo $status_exit

exit $status_exit
