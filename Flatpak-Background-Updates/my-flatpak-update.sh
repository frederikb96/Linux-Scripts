#!/bin/bash

# Update flatpaks within a timeout and capture any output
output=$(timeout 600s flatpak update -y 2>&1)

# Save the output to a file in the user's home directory
echo "$output" > "$HOME/.tmp-flatpak-update"

# Check the exit status of the timeout command
status=$?

# If the update was successful
if [ $status -eq 0 ]; then
    notify-send "Flatpak Update" "Update completed successfully."
# If the command timed out (status 124 from 'timeout')
elif [ $status -eq 124 ]; then
    notify-send "Flatpak Update" "Flatpak update took too long and may require manual intervention. Please check ~/.tmp-flatpak-update for details." -u critical
# If there was some other kind of error
else
    notify-send "Flatpak Update Error" "There was an error during the update. Please check ~/.tmp-flatpak-update for details." -u critical
fi
