#!/bin/bash

# get environment variables
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"

function send_email() {
    local subject="$1"
    local body="$2"
    echo -e "Subject:${subject}\n\n ${body}" | sendmail "$SENDMAIL_RECIPIENT"
}

function is_mounted() {
    mount | grep -q "$MOUNT_POINT"
    return $?
}

function connect_sshfs() {
    timeout 180s sshfs ${SOURCE_USER}@${SOURCE_HOST}:"${SOURCE_ADDRESS}" "$MOUNT_POINT"
}

case $1 in
  "--check")
    COUNTER=0

    while true; do
      if ! is_mounted; then
        # Try to reconnect
        connect_sshfs
        
        # Check every 10 seconds for up to 1 minute, so email is not issued for small connection hiccups
        while ! is_mounted && [ $COUNTER -lt 6 ]; do
            sleep 10
            COUNTER=$((COUNTER+1))
            connect_sshfs
        done

        # Reset the counter for the next round
        COUNTER=0

        # If it's still not mounted send the mail
        if ! is_mounted && [ ! -f "$ERROR_FLAG" ]; then
            send_email "WARNING Backup mount unavailable" "Please fix for ${SENDMAIL_DEVICE}!"
            touch "$ERROR_FLAG"
        elif is_mounted && [ -f "$ERROR_FLAG" ]; then
            [ -f "$ERROR_FLAG" ] && rm "$ERROR_FLAG"
            send_email "CLEARED Backup mount reconnected" "The backup mount for ${SENDMAIL_DEVICE} is reconnected successfully!"
        fi
        
      else
        [ -f "$ERROR_FLAG" ] && rm "$ERROR_FLAG"
      fi
      sleep 60
    done
    ;;
  "--stop")
    is_mounted && umount "$MOUNT_POINT"
    ;;
  "--cleanup")
    is_mounted && umount -lf "$MOUNT_POINT"
    ;;
  *)
    connect_sshfs
    ;;
esac
