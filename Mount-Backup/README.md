This service can be used to mount a backup drive via sshfs and periodically check if the drive is still mounted. If the drive is not mounted, it will try to mount it again. If the connection timeout is too long, it will send a mail to the specified address. If it is mounted again, it will send a mail that the drive is mounted again.

Setup
- Fill in the environment template and save as .env file
- Adjust the service file, if you dont use tailscale - you can remove the tailscale.service from the service file
- Also fill in the correct path to the script referenced in the service file
- Copy the service over to your services directory and enable it

```bash
rsync /root/Programming/linux-scripts/Mount-Backup/mount-backup.service /etc/systemd/system/mount-backup.service
sudo systemctl daemon-reload
sudo systemctl enable mount-backup.service
sudo systemctl start mount-backup.service
sudo systemctl status mount-backup.service
```