#!/bin/bash

# Get environment variables
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
source "${SCRIPT_PATH}/.env"

# Function to send email, if an error occurred and not already sent
function send_email() {
    echo "Sending email..."
    local subject="$1"
    local body="$2"
    echo -e "Subject:${subject}\n\n ${body}" | sendmail "$SENDMAIL_RECIPIENT"
}


# Stop function
function stop() {
    echo "Stopping..."
    if check; then
        umount $MOUNT_POINT ||
        cleanup
    else
        cleanup
    fi
}

# Cleanup function
function cleanup() {
    echo "Cleaning up..."
    umount -lf $MOUNT_POINT
}

# Check function
function check() {
    if ! mount | grep -q "$MOUNT_POINT"; then
        if [ ! -f "$ERROR_FLAG" ]; then
            echo "ERROR: Mount not available, sending email..."
            touch "$ERROR_FLAG"
            send_email "CRITICAL server $MOUNT_POINT Mount Unavailable" "The mount is unavailable."
        fi
        return 1
    fi
    return 0
}


# Startup function
function startup() {
    echo "Starting..."
    if mount | grep -q $MOUNT_POINT; then
        echo "During startup, found existing mounts, cleaning up and exiting..."
        cleanup
        exit 1
    fi

    echo "Startup process for mounts..."
    if ! timeout 180s rclone mount $MOUNT_SRC $MOUNT_POINT --allow-non-empty --vfs-cache-mode full --vfs-cache-max-size 100G --vfs-cache-max-age 720h --vfs-disk-space-total-size 5T --uid 0 --gid 0 --attr-timeout 168h --dir-cache-time 720h --poll-interval 168h --cache-dir $CACHE_DIR --daemon --daemon-wait 180s; then
        stop
        exit 1
    fi
    # Wait for mount to become really ready!
    sleep 5

    # Check if Docker is running
    if systemctl is-active --quiet docker; then
        # Check if the container is running
        if docker ps --filter "name=$CONTAINER" --filter "status=running" | grep -q $CONTAINER; then
            # Attempt to restart the container
            echo "Restarting $CONTAINER container..."
            docker restart $CONTAINER
            echo "Restarted $CONTAINER"
        fi
    else
        echo "Docker is not running. Skipping container check."
    fi

    echo "Startup mount process complete"

}

# Routine function
function routine() {
    echo "Routine..."
    systemd-notify --ready
    while true; do
        if ! check; then
            echo "Error occurred, cleaning up..."
            cleanup
            exit 1
        fi
        if [ -f "$ERROR_FLAG" ]; then
            echo "Clearing error flag..."
            send_email "CLEAR server $MOUNT_POINT available again" "Up and running again. Check for integrity yourself"
            # remove flag if it exists since cleared
            rm "$ERROR_FLAG"
        fi
        sleep 30
    done
}

# Parse command line arguments
case "$1" in
    --start)
        startup
        ;;
    --stop)
        stop
        ;;
    *)
        routine
        ;;
esac

