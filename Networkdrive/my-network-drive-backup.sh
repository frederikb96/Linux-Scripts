#!/bin/bash

set -x
case $1  in
  "--cleanup")
    echo "Cleanup"
	umount -lf /mnt/backup
	lvchange -an -f /dev/vg2/lv-1
	nbd-client -d /dev/nbd1
    ;;
  "--stop")
    echo "Stop"
	umount /mnt/backup
	lvchange -an /dev/vg2/lv-1
    ;;
  *)
    echo "Start"
	set -e
	sleep 1
	nbd-client storage.lan -N backup /dev/nbd1
	lvchange -ay /dev/vg2/lv-1
	mount /dev/mapper/vg2-lv--1 /mnt/backup
    ;;
esac
exit 0
