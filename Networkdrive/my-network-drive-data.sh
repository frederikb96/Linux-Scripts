#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")

set -x
case $1  in
  "--cleanup")
    echo "Cleanup"
	umount -lf /mnt/data
	lvchange -an -f /dev/vg1/lv-1
	cryptsetup close dm_crypt-2-cache
        cryptsetup close dm_crypt-2
	nbd-client -d /dev/nbd0
    ;;
  "--stop")
    echo "Stop"
	umount /mnt/data
	lvchange -an /dev/vg1/lv-1
	cryptsetup close dm_crypt-2-cache
	cryptsetup close dm_crypt-2
    ;;
  *)
    echo "Start"
	set -e
	sleep 1
	nbd-client storage.lan -N data /dev/nbd0
	cryptsetup open --key-file "${SCRIPT_PATH}/dm_crypt-2-cache.keyfile" /dev/vda3 dm_crypt-2-cache
	cryptsetup open --key-file "${SCRIPT_PATH}/dm_crypt-2.keyfile" /dev/nbd0 dm_crypt-2
	lvchange -ay /dev/vg1/lv-1
	mount /dev/mapper/vg1-lv--1 /mnt/data
    ;;
esac
exit 0
