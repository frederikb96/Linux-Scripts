#!/bin/bash

set -xe

sudo sed -i 's/ %U/ --use-tray-icon %U/' /usr/share/applications/signal-desktop.desktop

sudo update-desktop-database /usr/share/applications

echo "done"